import './App.css';
import Body from './Bodysection/Body';
import Footer from './Bodysection/Footer';
import Header from './Bodysection/Header';

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
