import React, { Component } from 'react'

export default class Header extends Component {
    render() {
        return (
            <>
                {/* NAV Bar */}
                <div className="bg-dark">
                    <div className="container">
                        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                            <div className="flex justify-content-between container">
                                <div>
                                    <a className="navbar-brand" href="#">Start Bootstrap</a>
                                </div>
                                <div>
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul className="navbar-nav mr-auto">
                                            <li className="nav-item active">
                                                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">About</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Contact</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>

                {/* Header */}
                <header className='py-5'>
                    <div className="container px-5">
                        <div className="p-5 bg-light rounded-3 text-center">
                            <div className="m-4">
                                <h1 className='font-weight-bold'>A warm welcome!</h1>
                                <h5 className='font-weight-normal'>Bootstrap utility classes are used to create this jumbotron since the
                                    old component has been removed from the framework.
                                    Why create custom CSS when you can use utilities?</h5>
                                <button className='btn btn-primary btn-lg mt-3'>Call to action</button>
                            </div>
                        </div>

                    </div>
                </header>

            </>
        )
    }
}
