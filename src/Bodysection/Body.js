import React, { Component } from 'react'
import Card from './Card'

export default class Body extends Component {
    render() {
        return (
            <>
                <div className="pt-4 container">
                    <div>
                        <div className="row mb-5">
                            <div className="col">
                                <Card />
                            </div>
                            <div className="col">
                                <Card />
                            </div>
                            <div className="col">
                                <Card />
                            </div>
                        </div>
                        <div className="row mb-5">
                            <div className="col">
                                <Card />
                            </div>
                            <div className="col">
                                <Card />
                            </div>
                            <div className="col">
                                <Card />
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
