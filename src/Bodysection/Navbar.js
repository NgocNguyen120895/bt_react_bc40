import React, { Component } from 'react'

export default class Navbar extends Component {
    render() {
        return (
            <div>  {/* NAV Bar */}
                <div className="bg-dark">
                    <div className="container">
                        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                            <div className="flex justify-content-between container">
                                <div>
                                    <a className="navbar-brand" href="#">Start Bootstrap</a>
                                </div>
                                <div>
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul className="navbar-nav mr-auto">
                                            <li className="nav-item active">
                                                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">About</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Contact</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div></div>
        )
    }
}
