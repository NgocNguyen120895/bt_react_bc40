import React, { Component } from 'react'

export default class Card extends Component {
    render() {
        return (
            <div>
                <div className="card bg-light border-0 m-auto" style={{ maxWidth: '18rem' }}>
                    <div className='bg-primary mx-auto my-0 mt-n4 text-white d-flex justify-content-center align-items-center' style={{ width: '65px', height: '65px', borderRadius: '5px' }} >
                        <i className="fa-sharp fa-regular fa-folder h2 mb-0" />
                    </div>
                    <div className="card-body">
                        <h2 className="card-title fw-bold">Fresh new layout</h2>
                        <p className="card-text mb-0">With Bootstrap 5, we've created a fresh new layout for this template!</p>
                    </div>
                </div>
            </div>
        )
    }
}
